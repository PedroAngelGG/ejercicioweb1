<?php
require_once "models/ProductosModel.php";
class ProductosController
{
	private $model;

	public function __construct(){
		$this->model = new ProductosModel();
	}

	public function index(){
		$productos = $this->model->obtenerProductos();

		require_once "views/lista_productos.php";
	}

	public function formulario_agregar(){
		$titulo = "Nuevo Producto"; 
		$accion = "agregar";
		require_once "views/formulario_producto.php";
	}

	public function formulario_actualizar(){
		$datos = $this->model->datosProducto($_GET['id']);
		if (!empty($datos)) {
			$titulo = "Actualizar Producto";
			$accion = "actualizar";
			require_once "views/formulario_producto.php";
		}
		else{
			$this->formulario_agregar();
		}
	}

	public function agregar(){
		$nombre = $_POST['nombre'];
		$precio = $_POST['precio'];

		$this->model->agregarProducto($nombre, $precio);

		$this->index();
		// header('index.php');
	}

	public function actualizar(){
		$id = $_POST['id'];
		$nombre = $_POST['nombre'];
		$precio = $_POST['precio'];

		$this->model->actualizarProducto($id, $nombre, $precio);

		$this->index();
	}

	public function eliminar(){
		$id = $_GET['id'];

		$this->model->eliminarProducto($id);

		$this->index();
	}
}

$productos_controller = new ProductosController();
?>
