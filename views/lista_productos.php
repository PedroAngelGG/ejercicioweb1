<h1>Productos</h1>
<button class="btn btn-primary" onClick="window.location.href='?c=productos&a=formulario_agregar'">Nuevo producto</button>
<table class="table table-bordered">
	<thead>
		<tr>
			<th>#</th>
			<th>Producto</th>
			<th>Precio</th>
			<th>IVA</th>
			<th>Precio + IVA</th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($productos as $producto) : ?>
		<tr>
			<?php foreach($producto as $value) : ?>
				<td><?php echo $value; ?></td>
			<?php endforeach; ?>
			<td>
				<button class="btn btn-success" onClick="window.location.href='?c=productos&a=formulario_actualizar&id=<?php echo $producto['id']?>'">
					Actualizar
				</button>
			</td>
			<td>
				<button class="btn btn-danger" onClick="window.location.href='?c=productos&a=eliminar&id=<?php echo $producto['id']?>'">
					Eliminar
				</button>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
