<h2><?php echo $titulo ?></h2>
<form class="row g-3" method="POST" action="<?php echo 'index.php?c=productos&a='.$accion; ?>">
	<input type="hidden" name="id" value="<?php echo $_GET['id']?? '' ?>">
	<div class="col-md-6">
		<label for="producto" class="form-label">Producto</label>
		<input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $datos['nombre'] ?? ''?>">
	</div>
	<div class="col-md-6">
		<label for="precio" class="form-label">Precio</label>
		<input type="number" class="form-control" id="precio" name="precio" step="0.01" min="0" value="<?php echo $datos['precio'] ?? 0 ?>">
	</div>
	<div class="col-12">
	    <button type="submit" class="btn btn-success">Guardar</button>
	</div>
</form>
