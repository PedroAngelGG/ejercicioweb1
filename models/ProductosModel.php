<?php
require('models/Conexion.php');

class ProductosModel
{
	private $db;
	function __construct()
	{
		$this->db = new Conexion();
	}

	/**
	* Consulta productos
	*
	* @author Pedro Garcia Glez. <garciaglez@outlook.com>
	* @return (array) lista de productos
	*/
	public function obtenerProductos(){
		$sql = "SELECT 
			id,
			nombre,
			CONCAT('$',FORMAT(precio,2)) precio,
			CONCAT('$',FORMAT(precio*0.16,2)) iva,
			CONCAT('$',FORMAT(precio+(precio*0.16),2)) precio_iva 
		FROM productos";
		return $this->db->Consulta($sql, [], 2);
	}

	/**
	* Consulta datos del producto
	*
	* @author Pedro Garcia Glez. <garciaglez@outlook.com>
	* @return (array) datos del producto
	*/
	public function datosProducto($id){
		$sql = "SELECT * FROM productos WHERE id=:id";
		$parametros = [
			'id' => $id
		];
		return $this->db->Consulta($sql, $parametros, 1);
	}

	/**
	* Inserta un nuevo producto
	*
	* @author Pedro Garcia Glez <garciaglez@outlook.com>
	* @param $nombre (string) Nombre del producto
	* @param $precio (float) Precio del producto
	* @return (boolean) 1=exito|0=error
	*/
	public function agregarProducto($nombre,$precio){
		$sql = "INSERT INTO productos SET nombre=:nombre, precio=:precio";
		$parametros = [
			'nombre' => $nombre,
			'precio' => $precio
		];
		return $this->db->Sentencia($sql,$parametros);
	}

	/**
	* Modifica un producto
	*
	* @author Pedro Garcia Glez <garciaglez@outlook.com>
	* @param $id (int) Identificador del producto
	* @param $nombre (string) Nombre del producto
	* @param $precio (float) Precio del producto
	* @return (boolean) 1=exito|0=error
	*/
	public function actualizarProducto($id,$nombre,$precio){
		$sql = "UPDATE productos SET nombre=:nombre, precio=:precio WHERE id=:id";
		$parametros = [
			'id' => $id,
			'nombre' => $nombre,
			'precio' => $precio
		];
		return $this->db->Sentencia($sql,$parametros);
	}

	/**
	* Eliminar un producto
	*
	* @author Pedro Garcia Glez <garciaglez@outlook.com>
	* @param $id (int) Identificador del producto
	* @return (boolean) 1=exito|0=error
	*/
	public function eliminarProducto($id){
		$sql = "DELETE FROM productos WHERE id=:id";
		$parametros = [
			'id' => $id
		];
		return $this->db->Sentencia($sql,$parametros);
	}
}
?>