<?php
/**
* Clase conexion a BD
*/
class Conexion
{
	//propiedades
	private $server="localhost";
	private $usuario="root";
	private $password="";
	private $DB="ejercicioweb1";

	//constructor
	public function __construct() 
	{
		//conexion PDO
		$dsn = "mysql:host=" . $this->server . ";dbname=" . $this->DB . ";charset=utf8;";
		$opciones = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
		//validamos que no haya errores en la conexion
		try {
			$this->conexion = new PDO($dsn, $this->usuario, $this->password, $opciones);
			//echo "Conexion exitosa"; 
		}
		catch(PDOException $e){
		    echo "Fallo conexion: " . $e->getMessage();
		    die();
		}
	}

	//metodos
	public function Consulta($sql, $parametros, $retorno=2){
		//validamos que no haya errores
		try {
			$sth = $this->conexion->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)); 
			foreach ($parametros as $clave => $valor)
			{	
				$sth->bindValue(":$clave", $valor);
			}//Prepara una sentencia para su ejecución y devuelve un objeto sentencia
			$sth->execute(); //Ejecuta una sentencia preparada
			if ($retorno==2) {
				$result = $sth->fetchAll(PDO::FETCH_ASSOC);
			}
			else{
				$result = $sth->fetch(PDO::FETCH_ASSOC);
			}
			$sth->closeCursor(); //Cierra un cursor, habilitando a la sentencia para que sea ejecutada otra vez
		}
		catch (Exception $e){
			echo "Error al ejecutar la consulta= ".$e->getMessage();
			die();
		}

		return $result;
	}

	public function Sentencia($sql,$parametros){
		//validamos que no haya errores
		try {
			$sth = $this->conexion->prepare($sql); //Prepara una sentencia para su ejecución y devuelve un objeto sentencia
			//recorre el arreglo de los parametros
			foreach ($parametros as $clave => $valor)
			{	
				$sth->bindValue(":$clave", $valor);
			}
			
			$result = $sth->execute(); //Ejecuta una sentencia preparada
			
			$sth->closeCursor(); //Cierra un cursor, habilitando a la sentencia para que sea ejecutada otra vez
		}
		catch (Exception $e){
			$result = "Error de ejecución= ". $e->getMessage();
			echo "Error al ejecutar la consulta= ".$e->getMessage();
			die();
		}

		return $result;
	}
}
?>