<?php
	function cargarControlador($controlador,$accion){
		$nombreControlador = ucwords($controlador)."Controller"; // identificar nombre de la clase del controlador
		
		$archivoControlador = "controllers/".$nombreControlador.".php"; // identificar nombre del archivo del controlador
		
		// si no se encuentra archivo cargara controlador por default
		if(!file_exists($archivoControlador)){
			$nombreControlador = "ProductosController";
			$archivoControlador = "controllers/ProductosController.php";
		}

		// echo "archivo: ".$archivoControlador."\n";
		// echo "clase: ".$nombreControlador."\n";
		// echo "metodo: ".$accion."\n";

		require_once $archivoControlador; // incluye archivo controlador
		$obj_controlador = new $nombreControlador(); // crea objeto controlador

		// validar que exista metodo
		if (method_exists($obj_controlador, $accion)) {
			return $obj_controlador->$accion(); // llama el metodo solicitado por la accion
		}
		else{
			return $obj_controlador->index(); // metodo default
		}
	}

?>